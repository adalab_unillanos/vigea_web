from django.contrib import admin

# Register your models here.
from usuario.models import Usuario


@admin.register(Usuario)
class UsuarioAdmin(admin.ModelAdmin):
    search_fields = ('user__username',)
    list_display = ('user', 'sexo', 'edad', 'vivienda', 'nivel', 'estadoUsuario')
    list_display_links = ('user', 'sexo', 'edad', 'vivienda', 'nivel', 'estadoUsuario')
    raw_id_fields = ('user',)
    # raw_id_fields = ('user', 'vivienda', 'nivel', 'estadoUsuario')