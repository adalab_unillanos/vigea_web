# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.contrib.gis.db import models
#17 modelos
# Create your models here.
from django.db.models.signals import post_save
from django.dispatch import receiver

from ubicacion.models import ComunaBarrio

sexo = (
    ('masculino', 'Masculino'), ('femenino', 'Femenino')
)

tipo_documento = (
    ('cc', 'Cedula de Ciudadania'), ('ti', 'Tarjeta de Identidad')
)

class TipoAgua(models.Model):
    nombre = models.CharField(max_length=45, verbose_name='nombre')

    def __str__(self):
        return self.nombre

class TipoPatio(models.Model):
    nombre = models.CharField(max_length=45, verbose_name='nombre')

    def __str__(self):
        return self.nombre

class Vivienda(models.Model):
    direccion = models.CharField(max_length=45, verbose_name='direccion')
    localizacion = models.PointField(verbose_name='localizacion')
    comunaBarrio = models.ForeignKey(ComunaBarrio)
    tipoAgua = models.ForeignKey(TipoAgua)
    tipoPatio = models.ForeignKey(TipoPatio)

    def __str__(self):
        return "vivienda con direccion: " + self.direccion

class Parentesco(models.Model):
    nombre = models.CharField(max_length=45, verbose_name='nombre')

    def __str__(self):
        return self.nombre

class EstadoUsuario(models.Model):
    estado = models.CharField(max_length=45, verbose_name='estado')

    def __str__(self):
        return self.estado

class TipoMascota(models.Model):
    tipo = models.CharField(max_length=45, verbose_name='tipo')

    def __str__(self):
        return self.tipo

class Nivel(models.Model):
    nombre = models.CharField(max_length=45, verbose_name='nombre')
    puntajeInicial = models.IntegerField(verbose_name='puntaje inicial')
    puntajeFinal = models.IntegerField(verbose_name='puntaje final')

    def __str__(self):
        return self.nombre

class Usuario(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    sexo = models.CharField(max_length=10, choices=sexo, blank=True, null=True)
    edad = models.IntegerField(blank=True, null=True)
    vivienda = models.ForeignKey(Vivienda, blank=True, null=True)
    nivel = models.ForeignKey(Nivel, blank=True, null=True)
    estadoUsuario = models.ForeignKey(EstadoUsuario, blank=True, null=True)

    def __str__(self):
        return self.user.first_name + " " + self.user.last_name

#Metodo que se ejecuta al crear o actualizar Usuario
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Usuario.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.usuario.save()

class Familia(models.Model):
    cantidadFamiliares = models.IntegerField(verbose_name='cantidad familiares')
    cantidadMascotas = models.IntegerField(verbose_name='cantidad mascotas')
    vivienda = models.ForeignKey(Vivienda)

    def __str__(self):
        return "casa #" + str(self.id)

class Mascota(models.Model):
    tipoMascota = models.ForeignKey(TipoMascota)
    familia = models.ForeignKey(Familia)

    def __str__(self):
        return "mascota #" + str(self.id)

class MiembroFamilia(models.Model):
    identificacion = models.IntegerField(verbose_name='identificación')
    nombre = models.CharField(max_length=45, verbose_name='nombre')
    apellido = models.CharField(max_length=45, verbose_name='apellido')
    parentesco = models.ForeignKey(Parentesco)
    sexo = models.CharField(max_length=10, choices=sexo)
    edad = models.IntegerField()
    familia = models.ForeignKey(Familia)
    tipoDocumento = models.CharField(max_length=10, choices=tipo_documento)

    def __str__(self):
        return self.nombre + self.apellido

class Premio(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='nombre')
    puntajeInicial = models.IntegerField(verbose_name='puntaje inicial')
    puntajeFinal = models.IntegerField(verbose_name='puntaje final')

    def __str__(self):
        return self.nombre

class PremioComoUsuario(models.Model):
    premio = models.ForeignKey(Premio)
    usuario = models.ForeignKey(Usuario)

    def __str__(self):
        return "premio " +self.premio.nombre + " al usuario " +self.usuario.nombre

    class Meta:
        unique_together = ('premio', 'usuario')