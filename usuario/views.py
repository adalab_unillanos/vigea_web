import random
import string

from django.shortcuts import render

# Create your views here.
def id_generator():
    return ''.join(random.choice(string.ascii_uppercase + string.digits + string.ascii_lowercase) for _ in range(12))