from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.serializers import Serializer

from usuario.models import Usuario


class UserSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    first_name = serializers.StringRelatedField()
    last_name = serializers.StringRelatedField()

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name')

class UsuarioSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    sexo = serializers.StringRelatedField()
    edad = serializers.StringRelatedField()
    vivienda = serializers.StringRelatedField()
    nivel = serializers.StringRelatedField()
    estadoUsuario = serializers.StringRelatedField()

    class Meta:
        model = Usuario
        fields = ('user', 'sexo', 'edad', 'vivienda', 'nivel', 'estadoUsuario')

class RegistroSerializer(Serializer):
    correo = serializers.CharField(max_length=150)
    password = serializers.CharField()


class CambiarContrasenaSerializer(Serializer):
    correo = serializers.EmailField()
    pass_antigua = serializers.CharField(max_length=150)
    pass_nueva = serializers.CharField(max_length=150)


class RecuperarContrasenaSerializer(Serializer):
    correo = serializers.EmailField()