from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.http import HttpResponseBadRequest
from rest_framework.authentication import BasicAuthentication, SessionAuthentication, TokenAuthentication
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from usuario.models import Usuario
from usuario.serializers import UsuarioSerializer, RegistroSerializer, CambiarContrasenaSerializer, \
    RecuperarContrasenaSerializer
from usuario.views import id_generator


class VerUsuarioAPIView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        username = request.GET.get('username')
        if username:
            user = get_object_or_404(User, username=username)
            usuario = get_object_or_404(Usuario, user=user)
            serialized_data = UsuarioSerializer(usuario)
            return Response(serialized_data.data, status=200)
        else:
            return HttpResponseBadRequest

class RegistrarUsuarioAPIView(APIView):

    permission_classes = (AllowAny,)

    def post(self, request, format=None):

        serializer = RegistroSerializer(data=request.data)

        if serializer.is_valid():
            datos = serializer.validated_data
            nuevo_usuario = User()
            nuevo_usuario.username = datos['correo']
            nuevo_usuario.email = datos['correo']
            nuevo_usuario.set_password(datos['password'])
            nuevo_usuario.save()
            return Response({'detail': 'Se ha creado el usuario correctamente'}, status=200)
        else:
            return HttpResponseBadRequest


class CambiarContrasenaAPIView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        serializer = CambiarContrasenaSerializer(data=request.data)

        if serializer.is_valid():
            datos = serializer.validated_data
            usuario = get_object_or_404(Usuario, user__email=datos['correo'])
            if usuario.user.check_password(datos['pass_antigua']):
                usuario.user.set_password(datos['pass_nueva'])
                usuario.user.save()
                return Response({'detail': 'La contraseña se ha actualizado correctamente'}, status=200)
            else:
                return Response({'detail': 'La contraseña antigua no es la correcta'}, status=400)
        else:
            return HttpResponseBadRequest


class RecuperarContrasenaAPIView(APIView):

    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        serializer = RecuperarContrasenaSerializer(data=request.data)

        if serializer.is_valid():
            datos = serializer.validated_data
            usuario = get_object_or_404(Usuario, user__email=datos['correo'])
            clave = id_generator()
            usuario.user.set_password(clave)
            usuario.user.save()
            send_mail('Recuperar Contraseña WebGIS Aedes Aegypti',
                      'Hola.\nPara ingresar nuevamente a tu cuenta ingresa esta nueva contraseña:\n'
                      +clave +'\n'
                      'Saludos.', 'soporte@webgisaedes.com', ['%s'%usuario.user.email])
            return Response({'detail': 'Ha sido enviada una nueva contraseña a tu correo electrónico, por favor revisa'},
                            status=200)
        else:
            return HttpResponseBadRequest