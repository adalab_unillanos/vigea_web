#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models
from django.contrib.gis.db import models

# Create your models here.
valido_choices = (
    (None, "No revisado"),
    (True, "Si"),
    (False, "No"),
)

class ReportarEstadio(models.Model):
    # modelo mediante el que se haran los reportes de estadio
    estadio_opciones = (
        ('larva', 'Larva'), ('pupa', 'Pupa'), ('zancudo', 'Zancudo')
    )
    usuario_creador = models.ForeignKey(User)
    imagen = models.ImageField(upload_to='reporte_estadio/%Y/%m/')
    fecha = models.DateTimeField(verbose_name="Fecha en la que se tomo el reporte")
    localizacion = models.PointField()
    descripcion = models.TextField(blank=True, null=True)
    cantidad = models.IntegerField()
    estadio = models.CharField(max_length=8, choices=estadio_opciones)
    fecha_creacion = models.DateTimeField(auto_now_add=True, verbose_name="Fecha en la que se almaceno el reporte")
    valido = models.NullBooleanField(choices=valido_choices, default=None, verbose_name="Este reporte es valido?")

    # Mostrar icono de booleano
    def display_valido(self):
        return self.valido

    display_valido.short_description = "Valido"
    display_valido.boolean = True

    #Metodo para poder mostrar imagen en el admin
    def display_img(self):
        return '<a href="%s" target="_blank"><img src="%s" class="img-responsive" style="width: 600px;"/></a>'%(
            self.imagen.url,self.imagen.url)
    display_img.short_description = 'Foto'
    display_img.allow_tags = True

    #Mostrar coordenadas donde se guardo el punto
    def display_coordenadas(self):
        return u"Latitud: %s - Longitud: %s"%(self.localizacion.y, self.localizacion.x)
    display_coordenadas.short_description = 'Coordenadas'
    display_coordenadas.allow_tags = False

    def __str__(self):
        return "reporte # %s" %self.id

    class Meta:
        verbose_name = "reporte de estadio"
        verbose_name_plural = "reportes de estadio"


class ReportarCriadero(models.Model):
    criadero_opciones = (
        ('llanta', 'Llanta'), ('tanque', 'Tanque'), ('lavadero', 'Lavadero'),
        ('florero', 'Florero'), ('recipiente', 'Recipiente'), ('botella', 'Botella'),
        ('otro', 'Otro')
    )
    usuario_creador = models.ForeignKey(User)
    criadero = models.CharField(max_length=11, choices=criadero_opciones)
    fecha = models.DateTimeField(verbose_name="Fecha en la que se tomo el reporte")
    localizacion = models.PointField()
    descripcion = models.TextField(blank=True, null=True)
    cantidad = models.IntegerField()
    fecha_creacion = models.DateTimeField(auto_now_add=True, verbose_name="Fecha en la que se almaceno el reporte")
    valido = models.NullBooleanField(choices=valido_choices, default=None, verbose_name="Este reporte es valido?")

    # Mostrar coordenadas donde se guardo el punto
    def display_coordenadas(self):
        return u"Latitud: %s - Longitud: %s" % (self.localizacion.y, self.localizacion.x)

    # Mostrar icono de booleano
    def display_valido(self):
        return self.valido
    display_valido.short_description = "Valido"
    display_valido.boolean = True

    display_coordenadas.short_description = 'Coordenadas'
    display_coordenadas.allow_tags = False

    def __str__(self):
        return "Reporte de criadero id #%s" %self.id

    class Meta:
        verbose_name = "Reporte de criadero"
        verbose_name_plural = "Reportes de criaderos"


class ReportarSintoma(models.Model):
    sintomas_opciones = (
        ('dolor_muscular', 'Dolor muscular'), ('dolor_cabeza', 'Dolor de cabeza'),
        ('dolor_estomacal', 'Dolor estomacal'), ('fiebre', 'Fiebre'), ('tos', 'Tos por mas de 15 dias'),
        ('sangrado', 'Sangrado'), ('otro', 'Otro')
    )
    quinquenio_opciones = (
        (0, '0-4'), (5, '5-9'), (10, '10-14'), (15, '15-19'), (20, '20-24'), (25, '25-29'), (30, '30-34'), (35, '35-39'),
        (40, '40-44'), (45, '45-49'), (50, '50-54'), (55, '55-59'), (60, '60-64'), (65, '65-69'), (70, '70-74'),
        (75, '75-79'), (80, '80-84'), (85, '85-89'), (90, '90-94'), (95, '95-99')
    )

    usuario_creador = models.ForeignKey(User)
    sintomas = models.CharField(max_length=16, choices=sintomas_opciones)
    descripcion = models.TextField(blank=True, null=True)
    fecha = models.DateTimeField(verbose_name="Fecha en la que se tomo el reporte")
    localizacion = models.PointField()
    fecha_inicio_sintomas = models.DateTimeField(verbose_name="Fecha en la que inicio el sintoma")
    fecha_creacion = models.DateTimeField(auto_now_add=True, verbose_name="Fecha en la que se almaceno el reporte")
    valido = models.NullBooleanField(choices=valido_choices, default=None, verbose_name="Este reporte es valido?")
    quinquenio = models.IntegerField(choices=quinquenio_opciones, default=0, verbose_name="Quinquenio en el oscila la edad de quien padece el sintoma")

    # Mostrar coordenadas donde se guardo el punto
    def display_coordenadas(self):
        return u"Latitud: %s - Longitud: %s" % (self.localizacion.y, self.localizacion.x)

    display_coordenadas.short_description = 'Coordenadas'
    display_coordenadas.allow_tags = False

    # Mostrar icono de booleano
    def display_valido(self):
        return self.valido

    display_valido.short_description = "Valido"
    display_valido.boolean = True

    def __str__(self):
        return "Reporte de sintomas id #%s" %self.id

    class Meta:
        verbose_name = "Reporte de sintomas"
        verbose_name_plural = "Reportes de sintomas"