from django.contrib import admin, messages

# Register your models here.
from django.contrib.gis.admin import GeoModelAdmin

from reporte.models import ReportarEstadio, ReportarCriadero, ReportarSintoma

def reporte_valido(ModelAdmin, request, queryset):
    for data in queryset:
        data.valido = True
        data.save()

    messages.set_level(request, messages.SUCCESS)
    messages.success(request, '%s reporte/s han cambiado su estado a VALIDO' % len(queryset))
reporte_valido.short_description = "Cambiar estado del reporte a valido"

def reporte_no_valido(ModelAdmin, request, queryset):
    for data in queryset:
        data.valido = False
        data.save()

    messages.set_level(request, messages.SUCCESS)
    messages.success(request, '%s reporte/s han cambiado su estado a NO VALIDO' % len(queryset))
reporte_no_valido.short_description = "Cambiar estado del reporte a no valido"


@admin.register(ReportarEstadio)
class ReportarEstadioAdmin(GeoModelAdmin):
    list_display = ('usuario_creador', 'estadio', 'fecha', 'display_coordenadas', 'cantidad', 'display_valido')
    list_display_links = ('usuario_creador', 'estadio', 'fecha', 'display_coordenadas', 'cantidad', 'display_valido')
    list_filter = ('estadio', 'fecha', 'valido',)
    raw_id_fields = ('usuario_creador',)
    search_fields = ('usuario_creador__username',)
    default_lon = -73.67706
    default_lat = 4.19403
    default_zoom = 4
    actions = [reporte_valido, reporte_no_valido]

    def change_view(self, request, object_id, form_url='', extra_context=None):
        if not request.user.is_superuser:
            # self.fields = ('display_img', 'fecha', 'display_coordenadas', 'localizacion', 'descripcion',
            #                'cantidad', 'estadio', 'valido')
            # self.exclude = ('usuario_creador',)
            # self.readonly_fields = ('fecha', 'display_img', 'display_coordenadas', 'descripcion',
            #                         'cantidad', 'estadio')
            self.fields = ('display_img', 'display_coordenadas', 'localizacion', 'fecha', 'descripcion', 'cantidad',
                           'estadio', 'valido')
            self.readonly_fields = ('display_img', 'display_coordenadas', 'fecha', 'descripcion', 'cantidad',
                           'estadio',)
            self.exclude = ('usuario_creador',)
            self.modifiable = False  # PARA NO PODER CAMBIAR LA LOCALIZACION DEL REPORTE
        else:
            self.fields = ('usuario_creador', 'fecha', 'imagen', 'localizacion', 'descripcion', 'cantidad', 'estadio',
                           'valido')
            self.readonly_fields = ('fecha_creacion',)
        return super(ReportarEstadioAdmin, self).change_view(request, object_id, form_url='', extra_context=None)



@admin.register(ReportarCriadero)
class ReportarCriaderoAdmin(GeoModelAdmin):
    list_display = ('usuario_creador', 'criadero', 'fecha', 'display_coordenadas', 'cantidad', 'display_valido')
    list_display_links = ('usuario_creador', 'criadero', 'fecha', 'display_coordenadas', 'cantidad', 'display_valido')
    list_filter = ('criadero', 'fecha', 'valido',)
    raw_id_fields = ('usuario_creador',)
    search_fields = ('usuario_creador__username',)
    default_lon = -73.67706
    default_lat = 4.19403
    default_zoom = 4
    actions = [reporte_valido, reporte_no_valido]

    def change_view(self, request, object_id, form_url='', extra_context=None):
        if not request.user.is_superuser:
            self.fields = ('fecha', 'display_coordenadas', 'localizacion', 'descripcion', 'cantidad', 'fecha_creacion',
                           'valido',)
            self.exclude = ('usuario_creador',)
            self.readonly_fields = ('fecha', 'display_coordenadas', 'descripcion', 'cantidad',
                                    'fecha_creacion',)
            self.modifiable = False
        else:
            self.fields = ('usuario_creador', 'criadero', 'fecha', 'localizacion', 'descripcion', 'cantidad', 'valido')
            self.readonly_fields = ('fecha_creacion',)
        return super(ReportarCriaderoAdmin, self).change_view(request, object_id, form_url='', extra_context=None)


@admin.register(ReportarSintoma)
class ReportarSintomaAdmin(GeoModelAdmin):
    list_display = ('usuario_creador', 'sintomas', 'fecha', 'fecha_inicio_sintomas',
                    'display_coordenadas', 'display_valido')
    list_display_links = ('usuario_creador', 'sintomas', 'fecha', 'fecha_inicio_sintomas',
                    'display_coordenadas', 'display_valido')
    list_filter = ('sintomas', 'fecha', 'fecha_inicio_sintomas', 'valido',)
    raw_id_fields = ('usuario_creador',)
    search_fields = ('usuario_creador__username',)
    default_lon = -73.67706
    default_lat = 4.19403
    default_zoom = 4
    actions = [reporte_valido, reporte_no_valido]

    def change_view(self, request, object_id, form_url='', extra_context=None):
        if not request.user.is_superuser:
            self.fields = ('fecha', 'sintomas', 'display_coordenadas', 'localizacion', 'fecha_inicio_sintomas',
                           'descripcion', 'quinquenio', 'valido')
            self.exclude = ('usuario_creador',)
            self.readonly_fields = ('fecha', 'sintomas', 'display_coordenadas', 'fecha_inicio_sintomas',
                           'descripcion', 'quinquenio')
            self.modifiable = False  # PARA NO PODER CAMBIAR LA LOCALIZACION DEL REPORTE
        else:
            self.fields = ('usuario_creador', 'sintomas', 'descripcion', 'fecha', 'localizacion',
                           'fecha_inicio_sintomas', 'quinquenio', 'valido')
            self.readonly_fields = ('fecha_creacion')
        return super(ReportarSintomaAdmin, self).change_view(request, object_id, form_url='', extra_context=None)