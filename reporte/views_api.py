#!/usr/bin/env python
# -*- coding: utf-8 -*-
import base64

from django.contrib.auth.models import User
from django.contrib.gis.geos import GEOSGeometry
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.base import ContentFile
from rest_framework import viewsets, filters
from rest_framework.authentication import TokenAuthentication
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from reporte.models import ReportarEstadio, ReportarCriadero, ReportarSintoma
from reporte.serializers import ReportarEstadioSerializer, ReportarCriaderoSerializer, ReportarSintomaSerializer, \
    VerReportarEstadioSerializer, VerReportarCriaderoSerializer, VerReportarSintomaSerializer


class ReportarEstadioAPIView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        serializer = ReportarEstadioSerializer(data=request.data)

        # VERIFICA SI LOS DATOS QUE LLEGARON SON DEL TIPO CORRECTO(STRING, FLOAT, INT, ETC)
        if serializer.is_valid():

            # INFORMACION QUE LLEGO POR POST
            datos = serializer.validated_data

            # DARLE FORMATO AL BASE64 PARA GUARDARLO COMO IMAGEN
            format, imgstr = datos['imagen'].split(';base64,')
            ext = format.split('/')[-1]
            data = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
            if not (ext == 'jpeg' or ext == 'png'):
                return Response({'detail': 'Solo se permiten archivos .jpeg o .png'}, status=400)
            # TERMINA DAR FORMATO AL BASE64

            # CREAR OBJETO TIPO REPORTAR ESTADIO
            reportar_estadio = ReportarEstadio()

            # VERIFICAR QUE EL USUARIO CREADOR DEL REPORTE EXISTA
            try:
                reportar_estadio.usuario_creador = User.objects.get(id=datos['usuario_creador'])
            except ObjectDoesNotExist:
                return Response({'detail': 'El usuario no existe'}, status=400)

            # VALIDAR TIPO DE ESTADIO
            estadio_opciones = ReportarEstadio.estadio_opciones
            existe = False
            for i in estadio_opciones:
                if i[0] == datos['estadio']:
                    existe = True
                    break
            if existe:
                reportar_estadio.estadio = datos['estadio']
            else:
                return Response({'detail': 'El tipo de estadio no existe'}, status=400)
            # TERMINA VALIDAR TIPO DE ESTADIO

            # INTENTAR INGRESAR TODOS LOS DATOS AL REPORTE Y LUEGO GUARDAR REPORTE
            try:
                reportar_estadio.imagen = data
                reportar_estadio.fecha = datos['fecha']
                reportar_estadio.localizacion = GEOSGeometry('POINT(' + str(datos['localizacion_y']) + ' '
                                                             + str(datos['localizacion_x']) + ')',
                                                             srid=4326)
                if 'descripcion' in datos:
                    reportar_estadio.descripcion = datos['descripcion']
                reportar_estadio.cantidad = datos['cantidad']
                reportar_estadio.estadio = datos['estadio']
                reportar_estadio.save()
                return Response({'detail': 'El reporte de estadio ha sido guardado correctamente'}, status=200)
            except:
                return Response({'detail': 'Hay un error en los datos y no hemos podido almacenarlo'}, status=400)
            # TERMINA INTENTAR INGRESAR TODOS LOS DATOS AL REPORTE Y LUEGO GUARDAR REPORTE

        else:
            # SI HAY ERRORES EN EL INGRESO DE DATOS IMPRIMIR NOMBRE DEL CAMPO ERRONEO
            # Y EL TIPO DE ERROR QUE TIENE DICHO CAMPO
            msn = ''
            for error in serializer.errors:
                msn = u"%s : %s"%(error,serializer.errors[error])
            return Response({'detail': msn}, status=400)


class ReportarCriaderoAPIView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        serializer = ReportarCriaderoSerializer(data=request.data)

        # VERIFICA SI LOS DATOS QUE LLEGARON SON DEL TIPO CORRECTO(STRING, FLOAT, INT, ETC)
        if serializer.is_valid():

            # INFORMACION QUE LLEGO POR POST
            datos = serializer.validated_data

            # CREAR OBJETO DONDE SE CARGARAN LOS DATOS QUE LLEGARON PRO POST
            reportar_criadero = ReportarCriadero()

            # VERIFICAR QUE EL USUARIO EXISTA
            try:
                reportar_criadero.usuario_creador = User.objects.get(pk=datos['usuario_creador'])
            except ObjectDoesNotExist:
                return Response({'detail': 'El usuario no existe'}, status=400)

            # VALIDAR TIPO DE CRIADERO
            criadero_opciones = ReportarCriadero.criadero_opciones
            existe = False
            for i in criadero_opciones:
                if i[0] == datos['criadero']:
                    existe = True
                    break
            if existe:
                reportar_criadero.criadero = datos['criadero']
            else:
                return Response({'detail': 'El tipo de criadero no existe'}, status=400)
            # TERMINA VALIDAR TIPO DE CRIADERO

            # INTENTAR CARGAR LOS DEMAS DATOS
            try:
                reportar_criadero.fecha = datos['fecha']
                reportar_criadero.localizacion = GEOSGeometry('POINT(' + str(datos['localizacion_y']) + ' '
                                                                 + str(datos['localizacion_x']) + ')',
                                                                 srid=4326)
                if 'descripcion' in datos:
                    reportar_criadero.descripcion = datos['descripcion']
                reportar_criadero.cantidad = datos['cantidad']
                reportar_criadero.save()
                return Response({'detail': 'El reporte de criadero ha sido guardado correctamente'}, status=200)
            except:
                return Response({'detail': 'Ha habido un error y no se ha podido guardar el reporte'}, status=400)
        else:
            # SI HAY ERRORES EN EL INGRESO DE DATOS IMPRIMIR NOMBRE DEL CAMPO ERRONEO
            # Y EL TIPO DE ERROR QUE TIENE DICHO CAMPO
            msn = ''
            for error in serializer.errors:
                msn = u"%s : %s" % (error, serializer.errors[error])
            return Response({'detail': msn}, status=400)


class ReportarSintomaAPIView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        serializer = ReportarSintomaSerializer(data=request.data)

        # VERIFICAR SI LOS DATOS QUE LLEGARON SON DEL TIPO QUE DEBEN SER
        if serializer.is_valid():

            # CAPTURAR DATOS QUE LLEGARON POR POST
            datos = serializer.validated_data

            # CREAR OBJETO EN EL QUE SE CARGARAN LOS DATOS QUE LLEGARON POR POST
            reportar_sintomas = ReportarSintoma()

            # VERIFICAR SI USUARIO EXISTE
            try:
                reportar_sintomas.usuario_creador = User.objects.get(pk=datos['usuario_creador'])
            except ObjectDoesNotExist:
                return Response({'detail': 'El usuario no existe'}, status=400)

            # VALIDAR TIPO DE SINTOMA
            sintomas_opciones = ReportarSintoma.sintomas_opciones
            existe = False
            for i in sintomas_opciones:
                if i[0] == datos['sintomas']:
                    existe = True
                    break
            if existe:
                reportar_sintomas.sintomas = datos['sintomas']
            else:
                return Response({'detail': 'El sintoma no existe'}, status=400)
            # TERMINA VALIDAR TIPO DE SINTOMA

            if 'descripcion' in request.POST:
                reportar_sintomas.descripcion = datos['descripcion']

            # VALIDAR TIPO DE QUINQUENIO
            quinquenio_opciones = ReportarSintoma.quinquenio_opciones
            existe = False
            for i in quinquenio_opciones:
                if i[0] == datos['quinquenio']:
                    existe = True
                    break
            if existe:
                reportar_sintomas.quinquenio = datos['quinquenio']
            else:
                return Response({'detail': 'El tipo de quinquenio no existe'}, status=400)
            # TERMINA VALIDAR TIPO DE QUINQUENIO


            # INTENTAR GUARDAR EL REPORTE
            try:
                reportar_sintomas.fecha = datos['fecha']
                reportar_sintomas.localizacion = GEOSGeometry('POINT(' + str(datos['localizacion_y']) + ' '
                                                                 + str(datos['localizacion_x']) + ')',
                                                                 srid=4326)
                reportar_sintomas.fecha_inicio_sintomas = datos['fecha_inicio_sintomas']
                reportar_sintomas.save()
                return Response({'detail': 'El reporte de sintomas ha sido guardado correctamente'}, status=200)
            except:
                return Response({'detail': 'Ha habido un error y no se ha podido guardar el reporte'}, status=400)
        else:
            # SI HAY ERRORES EN EL INGRESO DE DATOS IMPRIMIR NOMBRE DEL CAMPO ERRONEO
            # Y EL TIPO DE ERROR QUE TIENE DICHO CAMPO
            msn = ''
            for error in serializer.errors:
                msn = u"%s : %s" % (error, serializer.errors[error])
            return Response({'detail': msn}, status=400)


class ReportarEstadioViewSet(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = ReportarEstadio.objects.all()
    serializer_class = VerReportarEstadioSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('usuario_creador__id',)

class ReportarCriaderoViewSet(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = ReportarCriadero.objects.all()
    serializer_class = VerReportarCriaderoSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('usuario_creador__id',)

class ReportarSintomaViewSet(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = ReportarSintoma.objects.all()
    serializer_class = VerReportarSintomaSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('usuario_creador__id',)