#!/usr/bin/env python
# -*- coding: utf-8 -*-


# SERIALIZERS
import base64

from rest_framework import serializers
from rest_framework.serializers import ModelSerializer, Serializer

# SERIALIZER USADO EN ReportarEstadioAPIView
from reporte.models import ReportarEstadio, ReportarCriadero, ReportarSintoma
import pytz

from webgis_aedes_aegypti.settings import BASE_DIR


class ReportarEstadioSerializer(Serializer):

    usuario_creador = serializers.IntegerField()
    imagen = serializers.CharField()
    fecha = serializers.DateTimeField(format="%Y-%m-%d %H:%M") #EJEMPLO:"2018-(1-12)-(1-31) (1-23):(0-59)"
    localizacion_x = serializers.FloatField()
    localizacion_y = serializers.FloatField()
    descripcion = serializers.CharField(required=False)
    cantidad = serializers.IntegerField()
    estadio = serializers.CharField(max_length=8) #larva, pupa, zancudo


# SERIALIZER USADO EN ReportarCriaderoAPIView
class ReportarCriaderoSerializer(Serializer):

    usuario_creador = serializers.IntegerField()
    criadero = serializers.CharField(max_length=11) #
    fecha = serializers.DateTimeField(format="%Y-%m-%d %H:%M") #EJEMPLO:"2018-(1-12)-(1-31) (1-23):(0-59)"
    localizacion_x = serializers.FloatField()
    localizacion_y = serializers.FloatField()
    descripcion = serializers.CharField(required=False)
    cantidad = serializers.IntegerField()


# SERIALIZER USADO EN ReportarSintomaAPIView
class ReportarSintomaSerializer(Serializer):
    usuario_creador = serializers.IntegerField()
    sintomas = serializers.CharField(max_length=16)
    fecha = serializers.DateTimeField(format="%Y-%m-%d %H:%M")
    localizacion_x = serializers.FloatField()
    localizacion_y = serializers.FloatField()
    descripcion = serializers.CharField(required=False)
    fecha_inicio_sintomas = serializers.DateTimeField(format="%Y-%m-%d %H:%M")
    quinquenio = serializers.IntegerField()

# class VerReportarEstadioSerializer(serializers.ModelSerializer):
#     usuario_creador = serializers.StringRelatedField()
#     # imagen = serializers.SerializerMethodField()
#     fecha = serializers.SerializerMethodField()
#     latitud = serializers.SerializerMethodField()
#     longitud = serializers.SerializerMethodField()
#     descripcion = serializers.StringRelatedField()
#     cantidad = serializers.StringRelatedField()
#     estadio = serializers.StringRelatedField()
#     fecha_creacion = serializers.SerializerMethodField()
#
#     def get_latitud(self, objeto):
#         reporte = ReportarEstadio.objects.get(id=objeto.id)
#         return u'%s' % (reporte.localizacion.x)
#
#     def get_longitud(self, objeto):
#         reporte = ReportarEstadio.objects.get(id=objeto.id)
#         return u'%s' % (reporte.localizacion.y)
#
#     def get_fecha(self, objeto):
#         formato_fecha = '%Y-%m-%d %H:%M'
#         reporte = ReportarEstadio.objects.get(id=objeto.id)
#         fecha = reporte.fecha.astimezone(pytz.timezone('America/Bogota'))
#         return u"%s" % (fecha.strftime(formato_fecha))
#
#     def get_fecha_creacion(self, objeto):
#         formato_fecha = '%Y-%m-%d %H:%M'
#         reporte = ReportarEstadio.objects.get(id=objeto.id)
#         fecha = reporte.fecha_creacion.astimezone(pytz.timezone('America/Bogota'))
#         return u"%s" % (fecha.strftime(formato_fecha))
#
#     class Meta:
#         model = ReportarEstadio
#         fields = ('usuario_creador', 'fecha', 'latitud', 'longitud', 'descripcion', 'cantidad', 'estadio',
#                   'fecha_creacion',)

class VerReportarEstadioSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReportarEstadio
        fields = ('fecha', 'latitud', 'longitud', 'descripcion', 'cantidad', 'estadio',
                  'fecha_creacion', 'valido')

    #imagen = serializers.SerializerMethodField()
    fecha = serializers.SerializerMethodField()
    latitud = serializers.SerializerMethodField()
    longitud = serializers.SerializerMethodField()
    descripcion = serializers.StringRelatedField()
    cantidad = serializers.StringRelatedField()
    estadio = serializers.StringRelatedField()
    fecha_creacion = serializers.SerializerMethodField()
    valido = serializers.BooleanField()

    def get_latitud(self, objeto):
        reporte = ReportarEstadio.objects.get(id=objeto.id)
        return u'%s' % (reporte.localizacion.x)

    def get_longitud(self, objeto):
        reporte = ReportarEstadio.objects.get(id=objeto.id)
        return u'%s' % (reporte.localizacion.y)

    def get_fecha(self, objeto):
        formato_fecha = '%Y-%m-%d %H:%M'
        reporte = ReportarEstadio.objects.get(id=objeto.id)
        fecha = reporte.fecha.astimezone(pytz.timezone('America/Bogota'))
        return u"%s" % (fecha.strftime(formato_fecha))

    def get_fecha_creacion(self, objeto):
        formato_fecha = '%Y-%m-%d %H:%M'
        reporte = ReportarEstadio.objects.get(id=objeto.id)
        fecha = reporte.fecha_creacion.astimezone(pytz.timezone('America/Bogota'))
        return u"%s" % (fecha.strftime(formato_fecha))

    #def get_imagen(self, objeto):
        #reporte = ReportarEstadio.objects.get(id=objeto.id)
        # with open("/home/jorge/Trabajo/proyectos/webgis_aedes_aegypti/media/imagen.png", "rb") as image_file:
        #with open(BASE_DIR + objeto.imagen.url, "rb") as image_file:
        #    encoded_string = base64.b64encode(image_file.read())
        #return u"%s" % (encoded_string)

class VerReportarCriaderoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReportarCriadero
        fields = ('criadero', 'fecha', 'latitud', 'longitud', 'descripcion', 'cantidad', 'fecha_creacion', 'valido')

    criadero = serializers.StringRelatedField()
    fecha = serializers.SerializerMethodField()
    latitud = serializers.SerializerMethodField()
    longitud = serializers.SerializerMethodField()
    descripcion = serializers.StringRelatedField()
    cantidad = serializers.StringRelatedField()
    fecha_creacion = serializers.SerializerMethodField()
    valido = serializers.BooleanField()

    def get_latitud(self, objeto):
        reporte = ReportarCriadero.objects.get(id=objeto.id)
        return u'%s' % (reporte.localizacion.x)

    def get_longitud(self, objeto):
        reporte = ReportarCriadero.objects.get(id=objeto.id)
        return u'%s' % (reporte.localizacion.y)

    def get_fecha(self, objeto):
        formato_fecha = '%Y-%m-%d %H:%M'
        reporte = ReportarCriadero.objects.get(id=objeto.id)
        fecha = reporte.fecha.astimezone(pytz.timezone('America/Bogota'))
        return u"%s" % (fecha.strftime(formato_fecha))

    def get_fecha_creacion(self, objeto):
        formato_fecha = '%Y-%m-%d %H:%M'
        reporte = ReportarCriadero.objects.get(id=objeto.id)
        fecha = reporte.fecha_creacion.astimezone(pytz.timezone('America/Bogota'))
        return u"%s" % (fecha.strftime(formato_fecha))


class VerReportarSintomaSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReportarSintoma
        fields = ('sintomas', 'latitud', 'longitud', 'fecha', 'fecha_inicio_sintomas', 'descripcion', 'fecha_creacion', 'valido')

    sintomas = serializers.StringRelatedField()
    latitud = serializers.SerializerMethodField()
    longitud = serializers.SerializerMethodField()
    fecha = serializers.SerializerMethodField()
    fecha_inicio_sintomas = serializers.SerializerMethodField()
    descripcion = serializers.StringRelatedField()
    fecha_creacion = serializers.SerializerMethodField()
    valido = serializers.BooleanField()

    def get_latitud(self, objeto):
        reporte = ReportarSintoma.objects.get(id=objeto.id)
        return u'%s' % (reporte.localizacion.x)

    def get_longitud(self, objeto):
        reporte = ReportarSintoma.objects.get(id=objeto.id)
        return u'%s' % (reporte.localizacion.y)

    def get_fecha(self, objeto):
        formato_fecha = '%Y-%m-%d %H:%M'
        reporte = ReportarSintoma.objects.get(id=objeto.id)
        fecha = reporte.fecha.astimezone(pytz.timezone('America/Bogota'))
        return u"%s" % (fecha.strftime(formato_fecha))

    def get_fecha_inicio_sintomas(self, objeto):
        formato_fecha = '%Y-%m-%d %H:%M'
        reporte = ReportarSintoma.objects.get(id=objeto.id)
        fecha = reporte.fecha_inicio_sintomas.astimezone(pytz.timezone('America/Bogota'))
        return u"%s" % (fecha.strftime(formato_fecha))

    def get_fecha_creacion(self, objeto):
        formato_fecha = '%Y-%m-%d %H:%M'
        reporte = ReportarSintoma.objects.get(id=objeto.id)
        fecha = reporte.fecha_creacion.astimezone(pytz.timezone('America/Bogota'))
        return u"%s" % (fecha.strftime(formato_fecha))