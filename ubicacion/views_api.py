# -*- coding: utf-8 -*-
from django.contrib.gis.geos import GEOSGeometry
from rest_framework import serializers, viewsets, filters, permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from ubicacion.models import ComunaBarrio, Ciudad


class ComunaBarrioSerializer(serializers.ModelSerializer):
    nombre = serializers.StringRelatedField()
    localizacion_x = serializers.SerializerMethodField('traeLocalizacionX')
    localizacion_y = serializers.SerializerMethodField('traeLocalizacionY')
    ciudad = serializers.StringRelatedField()

    def traeLocalizacionX(self, objeto):
        aux = ComunaBarrio.objects.get(pk=objeto.id)
        return u'%s' %(aux.localizacion.y)

    def traeLocalizacionY(self, objeto):
        aux = ComunaBarrio.objects.get(pk=objeto.id)
        return u'%s' %(aux.localizacion.x)

    class Meta:
        model = ComunaBarrio
        fields = ('id', 'nombre', 'localizacion_x', 'localizacion_y', 'ciudad')


#API VIEWS
class ComunaBarrioViewSet(viewsets.ModelViewSet):
    queryset = ComunaBarrio.objects.all()
    serializer_class = ComunaBarrioSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('id',)

class InsertarComunaBarrioView(APIView):

    def get(self, request, format=None):
        nombre = request.GET.get("nombre")
        localizacion_x = request.GET.get("localizacion_x")
        localizacion_y = request.GET.get("localizacion_y")
        ciudad = request.GET.get("ciudad")

        if nombre and localizacion_x and localizacion_y and ciudad:
            try:
                comunaBarrio = ComunaBarrio()
                comunaBarrio.nombre = nombre
                comunaBarrio.localizacion = GEOSGeometry('POINT(' + localizacion_y + ' ' + localizacion_x + ')',
                                                         srid=4326)
                comunaBarrio.ciudad = Ciudad.objects.get(pk=ciudad)
                comunaBarrio.save()
                return Response({'detail' : u'Comuna o Barrio creado correctamente',}, status=200)
            except:
                return Response({'detail': u'No se pudo crear Comuna o Barrio',}, status=202)
        else:
            return Response({'detail': u'No ingresaste los datos correctamente', }, status=202)