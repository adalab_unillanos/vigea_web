# -*- coding: utf-8 -*-
import random

from django.contrib.gis.db import models
#4 modelos
# Create your models here.
from django.contrib.gis.geos import GEOSGeometry


class Pais(models.Model):
    class Meta:
        verbose_name = 'país'
        verbose_name_plural = 'países'

    nombre = models.CharField(max_length=45, verbose_name='nombre')

    def __str__(self):
        return self.nombre

class Departamento(models.Model):
    class Meta:
        verbose_name = 'departamento'
        verbose_name_plural = 'departamentos'

    nombre = models.CharField(max_length=45, verbose_name='nombre')
    pais = models.ForeignKey(Pais)

    def __str__(self):
        return self.nombre

class Ciudad(models.Model):
    class Meta:
        verbose_name = 'ciudad'
        verbose_name_plural = 'ciudades'

    nombre = models.CharField(max_length=45, verbose_name='nombre')
    departamento = models.ForeignKey(Departamento)

    def __str__(self):
        return self.nombre

class ComunaBarrio(models.Model):
    class Meta:
        verbose_name = 'comuna o barrio'
        verbose_name_plural = 'comunas o barrios'

    nombre = models.CharField(max_length=45, verbose_name='nombre')
    localizacion = models.PointField(verbose_name='localizacion')
    ciudad = models.ForeignKey(Ciudad)

    def __str__(self):
        return self.nombre