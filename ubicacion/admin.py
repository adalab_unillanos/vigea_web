from django.contrib import admin

# Register your models here.
from ubicacion.models import Pais, ComunaBarrio, Ciudad, Departamento

# @admin.register(Pais)
# class PaisAdmin(admin.ModelAdmin):
#     pass
from usuario.models import Usuario

@admin.register(ComunaBarrio)
class ComunaBarrioAdmin(admin.ModelAdmin):
    search_fields = ('nombre', 'ciudad__nombre', 'ciudad__departamento__nombre')
    list_display = ('nombre', 'ciudad')
    list_display_links = ('nombre', 'ciudad')
    raw_id_fields = ('ciudad',)

@admin.register(Ciudad)
class CiudadAdmin(admin.ModelAdmin):
    search_fields = ('nombre', 'departamento__nombre')
    list_display = ('nombre', 'departamento')
    list_display_links = ('nombre', 'departamento')
    raw_id_fields = ('departamento',)

@admin.register(Departamento)
class DepartamentoAdmin(admin.ModelAdmin):
    search_fields = ('nombre', 'pais__nombre')
    list_display = ('nombre', 'pais')
    list_display_links = ('nombre', 'pais')
    raw_id_fields = ('pais',)

@admin.register(Pais)
class PaisAdmin(admin.ModelAdmin):
    search_fields = ('nombre',)