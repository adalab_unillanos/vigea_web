run:
	./manage.py runserver

remigrar:
	./manage.py makemigrations
	./manage.py migrate

migrar:
	./manage.py migrate

mmigrar:
	./manage.py makemigrations informacion reporte ubicacion usuario

statics:
	./manage.py collectstatic

user:
	./manage.py createsuperuser