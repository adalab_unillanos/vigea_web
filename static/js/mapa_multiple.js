/*var comunas = new ol.layer.Heatmap
    ({
        title: 'Comunas o barrios',
        visible: true,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://localhost:8080/geoserver/gisdb/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=gisdb:' +
                'ubicacion_comunabarrio&maxFeatures=50&outputFormat=application%2Fjson'
        })
    });*/


console.log('asdasd');
var mapas = [];
mapas.push(
    new ol.layer.Tile
    ({
        title: 'Bing',
        type: 'base',
        visible: true,
        source: new ol.source.BingMaps
            ({
                key: 'ApkIrPCNvHd52C21dlwNQ-qlAvkuKkHqa1LqpZ9adurS5NbxuLCC9B1em_2SaUwJ',
                imagerySet: 'Aerial'
            })
    })
);
mapas.push(
    new ol.layer.Tile
    ({
        title: 'Open Street View',
        type: 'base',
        visible: true,
        source: new ol.source.OSM()
    })
);
var groupMapas = new ol.layer.Group({
    title: 'Mapas Base',
    layers: mapas
});

// Capa de Reportes de criadero
var criaderos = [];

criaderos.push(
    new ol.layer.Vector
    ({
        title: 'Llanta',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://190.60.95.13:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=' +
            'GetFeature&typeName=webgis_aedes:reportarcriadero_llanta&maxFeatures=50&outputFormat=application%2Fjson'
        })
    })
);

criaderos.push(
    new ol.layer.Vector
    ({
        title: 'Tanque',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://190.60.95.13:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
            'typeName=webgis_aedes:reportarcriadero_tanque&maxFeatures=50&outputFormat=application%2Fjson'
        })
    })
);

criaderos.push(
    new ol.layer.Vector
    ({
        title: 'Lavadero',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://190.60.95.13:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
            'typeName=webgis_aedes:reportarcriadero_lavadero&maxFeatures=50&outputFormat=application%2Fjson'
        })
    })
);

criaderos.push(
    new ol.layer.Vector
    ({
        title: 'Florero',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://190.60.95.13:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
            'typeName=webgis_aedes:reportarcriadero_florero&maxFeatures=50&outputFormat=application%2Fjson'
        })
    })
);

criaderos.push(
    new ol.layer.Vector
    ({
        title: 'Recipiente',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://190.60.95.13:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
            'typeName=webgis_aedes:reportarcriadero_recipiente&maxFeatures=50&outputFormat=application%2Fjson'
        })
    })
);

criaderos.push(
    new ol.layer.Vector
    ({
        title: 'Botella',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://190.60.95.13:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
            'typeName=webgis_aedes:reportarcriadero_botella&maxFeatures=50&outputFormat=application%2Fjson'
        })
    })
);

criaderos.push(
    new ol.layer.Vector
    ({
        title: 'Otro',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://190.60.95.13:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
            'typeName=webgis_aedes:reportarcriadero_otro&maxFeatures=50&outputFormat=application%2Fjson'
        })
    })
);

for(i=0; i<criaderos.length;i++){
    criaderos[i].setStyle(
        new ol.style.Style({
            image: new ol.style.Circle({
                fill: new ol.style.Fill({
                  color: 'rgba(0,255,0,0.8)'
                }),
                stroke: new ol.style.Stroke({
                  color: 'rgb(0,0,0)'
                }),
                radius: 5
            })
        })
    );
}

var groupCriadero = new ol.layer.Group({
    title: 'Reportes de criadero',
    layers: criaderos
});
// TERMINA CAPA DE REPORTES CRIADERO


// INICIA CAPA DE REPORTES ESTADIO

var estadios = [];

estadios.push(
    new ol.layer.Vector
    ({
        title: 'Larva',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://190.60.95.13:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
            'typeName=webgis_aedes:reportarestadio_larva&maxFeatures=50&outputFormat=application%2Fjson'
        })
    })
);

estadios.push(
    new ol.layer.Vector
    ({
        title: 'Pupa',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://190.60.95.13:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
            'typeName=webgis_aedes:reportarestadio_pupa&maxFeatures=50&outputFormat=application%2Fjson'
        })
    })
);

estadios.push(
    new ol.layer.Vector
    ({
        title: 'Zancudo',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://190.60.95.13:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
            'typeName=webgis_aedes:reportarestadio_zancudo&maxFeatures=50&outputFormat=application%2Fjson'
        })
    })
);

for(i=0; i<estadios.length;i++){
    estadios[i].setStyle(
        new ol.style.Style({
            image: new ol.style.Circle({
                fill: new ol.style.Fill({
                  color: 'rgba(0,255,0,0.4)'
                }),
                stroke: new ol.style.Stroke({
                  color: 'rgb(0,0,0)'
                }),
                radius: 5
            })
        })
    );
}

var groupEstadio = new ol.layer.Group({
    title: 'Reportes de estadio',
    layers: estadios
});
// TERMINA CAPA DE REPORTES ESTADIO

var map = new ol.Map
    ({
        target: 'map',
        render: 'canvas',
        layers: [],
        view: new ol.View({
            center: ol.proj.fromLonLat([-73.63, 4.14]),
            zoom:4
        })
    });

map.addLayer(groupMapas);
map.addLayer(groupCriadero);
map.addLayer(groupEstadio);

var layerSwitcher = new ol.control.LayerSwitcher({
    tipLabel: 'Leyenda'
});

map.addControl(layerSwitcher);
layerSwitcher.showPanel();
map.addControl(new ol.control.ZoomSlider());

var setBlendModeFromSelect = function(evt) {
    evt.context.globalCompositeOperation = 'luminosity';
};

var resetBlendModeFromSelect = function(evt) {
    evt.context.globalCompositeOperation = 'source-over';
};

var bindLayerListeners = function(layer) {
    layer.on('precompose', setBlendModeFromSelect);
    layer.on('postcompose', resetBlendModeFromSelect);
};

var unbindLayerListeners = function(layer) {
    layer.un('precompose', setBlendModeFromSelect);
    layer.un('postcompose', resetBlendModeFromSelect);
};

var affectLayerClicked = function() {
    var layer;
    for(i=0; i<criaderos.length; i++){
        layer = criaderos[i];
        bindLayerListeners(layer);
    }
    for(i=0; i<estadios.length; i++){
        layer = estadios[i];
        unbindLayerListeners(layer);
    }
    map.render();
};

affectLayerClicked();