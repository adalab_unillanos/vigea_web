"""webgis_aedes_aegypti URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import RedirectView, TemplateView
from rest_framework import routers

import informacion.views
from informacion.views import IndexView, NosotrosView, ContactoView, MapaMultipleView
from informacion.views_api import NoticiaViewSet, PrevencionViewSet, AyudaViewSet
from reporte.views_api import ReportarEstadioAPIView, ReportarCriaderoAPIView, ReportarSintomaAPIView, \
    ReportarEstadioViewSet, ReportarCriaderoViewSet, ReportarSintomaViewSet
from ubicacion.views_api import ComunaBarrioViewSet, InsertarComunaBarrioView
from rest_framework.authtoken import views as rest_framework_views

from usuario.views_api import VerUsuarioAPIView, RegistrarUsuarioAPIView, CambiarContrasenaAPIView, \
    RecuperarContrasenaAPIView
from webgis_aedes_aegypti import settings

router = routers.DefaultRouter()
#CASA API USERS Y TOKENS
#jorge 30e6924dcd4cb1cad120497fd7e291ce583fb7e2

#API UBICACION
router.register(r'comunabarrio', ComunaBarrioViewSet)

#API INFORMACION
router.register(r'noticia', NoticiaViewSet)
router.register(r'prevencion', PrevencionViewSet)
router.register(r'ayuda', AyudaViewSet)

#API REPORTE
router.register(r'ver-reportar-estadio', ReportarEstadioViewSet)
router.register(r'ver-reportar-criadero', ReportarCriaderoViewSet)
router.register(r'ver-reportar-sintoma', ReportarSintomaViewSet)

urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^mapa-multiple/$', MapaMultipleView.as_view(), name='mapa_multiple'),
    url(r'^contacto/$', ContactoView.as_view(), name='contacto'),
    url(r'^nosotros/$', NosotrosView.as_view(), name='nosotros'),
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls)),

    #Enviar Correos de Contacto
    url(r'^send_mail/$', informacion.views.EnviarCorreo, name='enviar_correo'),

    #API AUTENTIFICACION CON TOKEN
    url(r'^get_auth_token/$', rest_framework_views.obtain_auth_token, name='get_auth_token'),

    #API VIEWS UBICACION
    url(r'^setcomunabarrio/$', InsertarComunaBarrioView.as_view()),

    # API VIEWS REPORTE
    url(r'^api/reportar-estadio/$', ReportarEstadioAPIView.as_view(), name='reportar_estadio'),
    url(r'^api/reportar-criadero/$', ReportarCriaderoAPIView.as_view(), name='reportar_criadero'),
    url(r'^api/reportar-sintoma/$', ReportarSintomaAPIView.as_view(), name='reportar_sintoma'),

    #API USUARIO
    url(r'^api/registrar-usuario/$', RegistrarUsuarioAPIView.as_view(), name='registrar_usuario'),
    url(r'^api/ver-usuario/$', VerUsuarioAPIView.as_view(), name='ver_usuario'),
    url(r'^api/cambiar-password/$', CambiarContrasenaAPIView.as_view(), name='cambiar_password'),
    url(r'^api/recuperar-password/$', RecuperarContrasenaAPIView.as_view(), name='recuperar_password'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + \
              static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = "Gestión VIGEA"
admin.site.site_title = "VIGEA"