from django.contrib import admin

# Register your models here.
from informacion.models import Noticia, Prevencion, Ayuda


@admin.register(Noticia)
class NoticiaAdmin(admin.ModelAdmin):
    search_fields = ('titulo',)
    list_display = ('titulo', 'contenido', 'fecha_creacion',)


@admin.register(Prevencion)
class PrevencionAdmin(admin.ModelAdmin):
    search_fields = ('titulo',)
    list_display = ('titulo', 'contenido', 'fecha_creacion',)


@admin.register(Ayuda)
class AyudaAdmin(admin.ModelAdmin):
    search_fields = ('titulo',)
    list_display = ('titulo', 'contenido', 'fecha_creacion',)