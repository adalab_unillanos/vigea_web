# -*- coding: utf-8 -*-
from django.contrib.gis.geos import GEOSGeometry
from rest_framework import serializers, viewsets, filters, permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from informacion.models import Noticia, Prevencion, Ayuda
from informacion.serializers import NoticiaSerializer, PrevencionSerializer, AyudaSerializer
from ubicacion.models import ComunaBarrio, Ciudad

#API VIEWS
class NoticiaViewSet(viewsets.ModelViewSet):
    queryset = Noticia.objects.all()
    serializer_class = NoticiaSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('id',)
    # permission_classes = (permissions.AllowAny,)


class PrevencionViewSet(viewsets.ModelViewSet):
    queryset = Prevencion.objects.all()
    serializer_class = PrevencionSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('id',)
    # permission_classes = (permissions.AllowAny,)


class AyudaViewSet(viewsets.ModelViewSet):
    queryset = Ayuda.objects.all()
    serializer_class = AyudaSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('id',)
    # permission_classes = (permissions.AllowAny,)