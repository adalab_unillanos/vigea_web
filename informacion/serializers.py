# -*- coding: utf-8 -*-
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework import serializers
from rest_framework.authtoken.models import Token

from informacion.models import Noticia, Prevencion, Ayuda

#METODO PARA CREAR TOKEN CADA VEZ QUE CREEN UN USUARIO
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class NoticiaSerializer(serializers.ModelSerializer):
    titulo = serializers.StringRelatedField()
    contenido = serializers.SerializerMethodField('traerContenido')
    imagen = serializers.ImageField()
    fecha_creacion = serializers.DateTimeField()

    def traerContenido(self, objeto):
        aux = Noticia.objects.get(pk=objeto.id)
        return u'%s' % aux.contenido

    class Meta:
        model = Noticia
        fields = ('id', 'titulo', 'contenido', 'imagen', 'fecha_creacion',)

class PrevencionSerializer(serializers.ModelSerializer):
    titulo = serializers.StringRelatedField()
    contenido = serializers.SerializerMethodField('traerContenido')
    imagen = serializers.ImageField()
    fecha_creacion = serializers.DateTimeField()

    def traerContenido(self, objeto):
        aux = Prevencion.objects.get(pk=objeto.id)
        return u'%s' % aux.contenido

    class Meta:
        model = Prevencion
        fields = ('id', 'titulo', 'contenido', 'imagen', 'fecha_creacion',)


class AyudaSerializer(serializers.ModelSerializer):
    titulo = serializers.StringRelatedField()
    contenido = serializers.SerializerMethodField('traerContenido')
    imagen = serializers.ImageField()
    fecha_creacion = serializers.DateTimeField()

    def traerContenido(self, objeto):
        aux = Ayuda.objects.get(pk=objeto.id)
        return u'%s' % aux.contenido

    class Meta:
        model = Ayuda
        fields = ('id', 'titulo', 'contenido', 'imagen', 'fecha_creacion',)