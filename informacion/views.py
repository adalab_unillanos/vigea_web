from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView


class IndexView(TemplateView):
    template_name = "mapa.html"

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['mapa'] = True
        context['user'] = self.request.user
        return context

class MapaMultipleView(TemplateView):
    template_name = 'mapa_multiple.html'

    def get_context_data(self, **kwargs):
        context = super(MapaMultipleView, self).get_context_data(**kwargs)
        context['mapa_multiple'] = True
        context['user'] = self.request.user
        return context


class NosotrosView(TemplateView):
    template_name = "nosotros.html"

    def get_context_data(self, **kwargs):
        context = super(NosotrosView, self).get_context_data(**kwargs)
        context['nosotros'] = True
        context['user'] = self.request.user
        return context


class ContactoView(TemplateView):
    template_name = "contacto.html"

    def get_context_data(self, **kwargs):
        context = super(ContactoView, self).get_context_data(**kwargs)
        context['contacto'] = True
        context['user'] = self.request.user
        return context


def EnviarCorreo(request):
    if request.method == 'POST':
        nombre = request.POST.get('nombre')
        apellido = request.POST.get('apellido')
        mensaje = request.POST.get('mensaje')
        correo = request.POST.get('correo')
        if nombre and apellido and mensaje and correo:
            try:
                send_mail(u"Contactame. Enviado por: " +nombre +" " +apellido, mensaje, correo, ['webgisaedes@gmail.com'])
            except BadHeaderError:
                return HttpResponse('Encabezado no valido')
            return render(request, 'correo_enviado.html')
        else:
            return HttpResponseBadRequest