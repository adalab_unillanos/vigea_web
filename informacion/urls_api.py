from django.conf.urls import url, include

from ubicacion.views_api import InsertarComunaBarrioView

urlpatterns = [
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]