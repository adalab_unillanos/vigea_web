// VALORES MAXIMOS PARA EL WEIGHT DE LAS CAPAS DE ESTADIO
var max_larva = 0;
var max_pupa = 0;
var max_zancudo = 0;
var max_estadio = 0;

// RECUPERAR CANTIDAD MAXIMA EN REPORTES DE ESTADIO LARVA
$.getJSON(
    'http://vigea.unillanos.edu.co:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
    'typeName=webgis_aedes:reportarestadio_larva&maxFeatures=50&outputFormat=application%2Fjson',
    function (data) {
        array_larvas = [];
        data.features.forEach(function (element) {
            array_larvas.push(element.properties.cantidad);
        });
        max_larva = Math.max.apply(null, array_larvas);
        if(max_larva == -Infinity){
            max_larva = 1;
        }
    }
);

// RECUPERAR CANTIDAD MAXIMA EN REPORTES DE ESTADIO PUPA
$.getJSON(
    'http://vigea.unillanos.edu.co:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
    'typeName=webgis_aedes:reportarestadio_pupa&maxFeatures=50&outputFormat=application%2Fjson',
    function (data) {
        array_pupa = [];
        data.features.forEach(function (element) {
            array_pupa.push(element.properties.cantidad);
        });
        max_pupa = Math.max.apply(null, array_pupa);
        if(max_pupa == -Infinity){
            max_pupa = 1;
        }
    }
);

// RECUPERAR CANTIDAD MAXIMA EN REPORTES DE ESTADIO ZANCUDO
$.getJSON(
    'http://vigea.unillanos.edu.co:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
    'typeName=webgis_aedes:reportarestadio_zancudo&maxFeatures=50&outputFormat=application%2Fjson',
    function (data) {
        array_zancudos = [];
        data.features.forEach(function (element) {
            array_zancudos.push(element.properties.cantidad);
        });
        max_zancudo = Math.max.apply(null, array_zancudos);
        if(max_zancudo == -Infinity){
            max_zancudo = 1;
        }
    }
);

// RECUPERAR CANTIDAD MAXIMA EN REPORTES DE ESTADIO GENERAL
$.getJSON(
    'http://vigea.unillanos.edu.co:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
    'typeName=webgis_aedes:reportarestadio_general&maxFeatures=50&outputFormat=application%2Fjson',
    function (data) {
        array_zancudos = [];
        data.features.forEach(function (element) {
            array_zancudos.push(element.properties.cantidad);
        });
        max_estadio = Math.max.apply(null, array_zancudos);
        if(max_estadio == -Infinity){
            max_estadio = 1;
        }
    }
);
// TERMINA VALORES MAXIMOS PARA EL WEIGHT DE LAS CAPAS DE ESTADIO

// VALORES MAXIMOS PARA EL WEIGHT DE LAS CAPAS DE CRIADEROS
var max_llanta = 0;
var max_tanque = 0;
var max_lavadero = 0;
var max_florero = 0;
var max_recipiente = 0;
var max_botella = 0;
var max_otro = 0;
var max_criadero = 0;

// MAX LLANTA
$.getJSON(
    'http://vigea.unillanos.edu.co:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
    'typeName=webgis_aedes:reportarcriadero_llanta&maxFeatures=50&outputFormat=application%2Fjson',
    function (data) {
        aux = [];
        data.features.forEach(function (element) {
            aux.push(element.properties.cantidad);
        });
        max_llanta = Math.max.apply(null, aux);
        if(max_llanta == -Infinity){
            max_llanta = 1;
        }
    }
);

// MAX TANQUE
$.getJSON(
    'http://vigea.unillanos.edu.co:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
    'typeName=webgis_aedes:reportarcriadero_tanque&maxFeatures=50&outputFormat=application%2Fjson',
    function (data) {
        aux = [];
        data.features.forEach(function (element) {
            aux.push(element.properties.cantidad);
        });
        max_tanque = Math.max.apply(null, aux);
        if(max_tanque == -Infinity){
            max_tanque = 1;
        }
    }
);

// MAX LAVADERO
$.getJSON(
    'http://vigea.unillanos.edu.co:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=webgis_aedes:reportarcriadero_lavadero&maxFeatures=50&outputFormat=application%2Fjson',
    function (data) {
        aux = [];
        data.features.forEach(function (element) {
            aux.push(element.properties.cantidad);
        });
        max_lavadero = Math.max.apply(null, aux);
        if(max_lavadero == -Infinity){
            max_lavadero = 1;
        }
    }
);

// MAX FLORERO
$.getJSON(
    'http://vigea.unillanos.edu.co:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=webgis_aedes:reportarcriadero_florero&maxFeatures=50&outputFormat=application%2Fjson',
    function (data) {
        aux = [];
        data.features.forEach(function (element) {
            aux.push(element.properties.cantidad);
        });
        max_florero = Math.max.apply(null, aux);
        if(max_florero == -Infinity){
            max_florero = 1;
        }
    }
);

// MAX RECIPIENTE
$.getJSON(
    'http://vigea.unillanos.edu.co:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=webgis_aedes:reportarcriadero_recipiente&maxFeatures=50&outputFormat=application%2Fjson',
    function (data) {
        aux = [];
        data.features.forEach(function (element) {
            aux.push(element.properties.cantidad);
        });
        max_recipiente = Math.max.apply(null, aux);
        if(max_recipiente == -Infinity) {
            max_recipiente = 1;
        }
    }
);

// MAX BOTELLA
$.getJSON(
    'http://vigea.unillanos.edu.co:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=webgis_aedes:reportarcriadero_botella&maxFeatures=50&outputFormat=application%2Fjson',
    function (data) {
        aux = [];
        data.features.forEach(function (element) {
            aux.push(element.properties.cantidad);
        });
        max_botella = Math.max.apply(null, aux);
        if(max_botella == -Infinity){
            max_botella = 1;
        }
    }
);

// MAX OTRO
$.getJSON(
    'http://vigea.unillanos.edu.co:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=webgis_aedes:reportarcriadero_otro&maxFeatures=50&outputFormat=application%2Fjson',
    function (data) {
        aux = [];
        data.features.forEach(function (element) {
            aux.push(element.properties.cantidad);
        });
        max_otro = Math.max.apply(null, aux);
        if(max_otro == -Infinity){
            max_otro = 1;
        }
    }
);

// MAX CRIADERO
$.getJSON(
    'http://vigea.unillanos.edu.co:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=webgis_aedes:reportarcriadero_general&maxFeatures=50&outputFormat=application%2Fjson',
    function (data) {
        aux = [];
        data.features.forEach(function (element) {
            aux.push(element.properties.cantidad);
        });
        max_criadero = Math.max.apply(null, aux);
        if(max_criadero == -Infinity){
            max_criadero = 1;
        }
    }
);
// TERMINA VALORES MAXIMOS PARA EL WEIGHT DE LAS CAPAS DE CRIADEROS


// DECLARAR MAPAS BASE
var mapas = [];
mapas.push(
    new ol.layer.Tile
    ({
        title: 'Bing',
        type: 'base',
        visible: true,
        source: new ol.source.BingMaps
        ({
            key: 'ApkIrPCNvHd52C21dlwNQ-qlAvkuKkHqa1LqpZ9adurS5NbxuLCC9B1em_2SaUwJ',
            imagerySet: 'Aerial'
        })
    })
);

mapas.push(
    new ol.layer.Tile
    ({
        title: 'Open Street View',
        type: 'base',
        visible: true,
        source: new ol.source.OSM()
    })
);
var groupMapas = new ol.layer.Group({
    title: 'Mapas Base',
    layers: mapas
});
// TERMINA DECLARAR MAPAS BASE


// CAPA DE REPORTES DE CRIADERO
var criaderos = [];

criaderos.push(
    new ol.layer.Heatmap
    ({
        title: 'Otro',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://190.60.95.13:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
            'typeName=webgis_aedes:reportarcriadero_otro&maxFeatures=50&outputFormat=application%2Fjson'
        }),
        weight: function (feature) {
            var weightProperty = feature.get('cantidad');
            // perform some calculation to get weightProperty between 0 - 1
            weightProperty = weightProperty / max_otro; // this was your suggestion - make sure this makes sense
            return weightProperty;
        }
    })
);

criaderos.push(
    new ol.layer.Heatmap
    ({
        title: 'Tanque',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://190.60.95.13:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
            'typeName=webgis_aedes:reportarcriadero_tanque&maxFeatures=50&outputFormat=application%2Fjson'
        }),
        weight: function (feature) {
            var weightProperty = feature.get('cantidad');
            // perform some calculation to get weightProperty between 0 - 1
            weightProperty = weightProperty / max_tanque; // this was your suggestion - make sure this makes sense
            return weightProperty;
        }
    })
);

criaderos.push(
    new ol.layer.Heatmap
    ({
        title: 'Lavadero',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://190.60.95.13:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
            'typeName=webgis_aedes:reportarcriadero_lavadero&maxFeatures=50&outputFormat=application%2Fjson'
        }),
        weight: function (feature) {
            var weightProperty = feature.get('cantidad');
            // perform some calculation to get weightProperty between 0 - 1
            weightProperty = weightProperty / max_lavadero; // this was your suggestion - make sure this makes sense
            return weightProperty;
        }
    })
);

criaderos.push(
    new ol.layer.Heatmap
    ({
        title: 'Florero',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://190.60.95.13:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
            'typeName=webgis_aedes:reportarcriadero_florero&maxFeatures=50&outputFormat=application%2Fjson'
        }),
        weight: function (feature) {
            var weightProperty = feature.get('cantidad');
            // perform some calculation to get weightProperty between 0 - 1
            weightProperty = weightProperty / max_florero; // this was your suggestion - make sure this makes sense
            return weightProperty;
        }
    })
);

criaderos.push(
    new ol.layer.Heatmap
    ({
        title: 'Recipiente',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://190.60.95.13:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
            'typeName=webgis_aedes:reportarcriadero_recipiente&maxFeatures=50&outputFormat=application%2Fjson'
        }),
        weight: function (feature) {
            var weightProperty = feature.get('cantidad');
            // perform some calculation to get weightProperty between 0 - 1
            weightProperty = weightProperty / max_recipiente; // this was your suggestion - make sure this makes sense
            return weightProperty;
        }
    })
);

criaderos.push(
    new ol.layer.Heatmap
    ({
        title: 'Botella',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://190.60.95.13:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
            'typeName=webgis_aedes:reportarcriadero_botella&maxFeatures=50&outputFormat=application%2Fjson'
        }),
        weight: function (feature) {
            var weightProperty = feature.get('cantidad');
            // perform some calculation to get weightProperty between 0 - 1
            weightProperty = weightProperty / max_botella; // this was your suggestion - make sure this makes sense
            return weightProperty;
        }
    })
);

criaderos.push(
    new ol.layer.Heatmap
    ({
        title: 'Llanta',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://190.60.95.13:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=' +
            'GetFeature&typeName=webgis_aedes:reportarcriadero_llanta&maxFeatures=50&outputFormat=application%2Fjson'
        }),
        weight: function (feature) {
            var weightProperty = feature.get('cantidad');
            // perform some calculation to get weightProperty between 0 - 1
            weightProperty = weightProperty / max_llanta; // this was your suggestion - make sure this makes sense
            return weightProperty;
        }
    })
);



criaderos.push(
    new ol.layer.Heatmap
    ({
        title: 'Todos los criaderos',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://vigea.unillanos.edu.co:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
            'typeName=webgis_aedes:reportarcriadero_general&maxFeatures=50&outputFormat=application%2Fjson'
        }),
        weight: function (feature) {
            var weightProperty = feature.get('cantidad');
            // perform some calculation to get weightProperty between 0 - 1
            weightProperty = weightProperty / max_criadero; // this was your suggestion - make sure this makes sense
            return weightProperty;
        }
    })
);

var groupCriadero = new ol.layer.Group({
    title: 'Reportes de criadero',
    layers: criaderos
});
// TERMINA CAPA DE REPORTES DE CRIADERO


// INICIA CAPA DE REPORTES ESTADIO
var estadios = [];

estadios.push(
    new ol.layer.Heatmap
    ({
        title: 'Larva',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://190.60.95.13:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
            'typeName=webgis_aedes:reportarestadio_larva&maxFeatures=50&outputFormat=application%2Fjson'
        }),
        weight: function (feature) {
            var weightProperty = feature.get('cantidad');
            // perform some calculation to get weightProperty between 0 - 1
            weightProperty = weightProperty / max_larva; // this was your suggestion - make sure this makes sense
            return weightProperty;
        }
    })
);

estadios.push(
    new ol.layer.Heatmap
    ({
        title: 'Pupa',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://190.60.95.13:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
            'typeName=webgis_aedes:reportarestadio_pupa&maxFeatures=50&outputFormat=application%2Fjson'
        }),
        weight: function (feature) {
            var weightProperty = feature.get('cantidad');
            // perform some calculation to get weightProperty between 0 - 1
            weightProperty = weightProperty / max_pupa; // this was your suggestion - make sure this makes sense
            return weightProperty;
        }
    })
);

estadios.push(
    new ol.layer.Heatmap
    ({
        title: 'Zancudo',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://190.60.95.13:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
            'typeName=webgis_aedes:reportarestadio_zancudo&maxFeatures=50&outputFormat=application%2Fjson'
        }),
        weight: function (feature) {
            var weightProperty = feature.get('cantidad');
            // perform some calculation to get weightProperty between 0 - 1
            weightProperty = weightProperty / max_zancudo; // this was your suggestion - make sure this makes sense
            return weightProperty;
        }
    })
);

estadios.push(
    new ol.layer.Heatmap
    ({
        title: 'Todos los estadios',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://190.60.95.13:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&' +
            'typeName=webgis_aedes:reportarestadio_general&maxFeatures=50&outputFormat=application%2Fjson'
        }),
        weight: function (feature) {
            var weightProperty = feature.get('cantidad');
            // perform some calculation to get weightProperty between 0 - 1
            weightProperty = weightProperty / max_estadio; // this was your suggestion - make sure this makes sense
            return weightProperty;
        }
    })
);

var groupEstadio = new ol.layer.Group({
    title: 'Reportes de estadio',
    layers: estadios
});
// TERMINA CAPA DE REPORTES ESTADIO


// INICIA CAPA DE REPORTES SINTOMAS
var sintoma = [];

sintoma.push(
    new ol.layer.Heatmap
    ({
        title: 'Otro',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://vigea.unillanos.edu.co:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=webgis_aedes:reportarsintoma_otro&maxFeatures=50&outputFormat=application%2Fjson'
        })
    })
);

sintoma.push(
    new ol.layer.Heatmap
    ({
        title: 'Dolor Muscular',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://vigea.unillanos.edu.co:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=webgis_aedes:reportarsintoma_dolor_muscular&maxFeatures=50&outputFormat=application%2Fjson'
        })
    })
);

sintoma.push(
    new ol.layer.Heatmap
    ({
        title: 'Dolor Muscular',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://vigea.unillanos.edu.co:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=webgis_aedes:reportarsintoma_dolor_muscular&maxFeatures=50&outputFormat=application%2Fjson'
        })
    })
);

sintoma.push(
    new ol.layer.Heatmap
    ({
        title: 'Dolor Cabeza',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://vigea.unillanos.edu.co:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=webgis_aedes:reportarsintoma_dolor_cabeza&maxFeatures=50&outputFormat=application%2Fjson'
        })
    })
);

sintoma.push(
    new ol.layer.Heatmap
    ({
        title: 'Dolor Estomacal',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://vigea.unillanos.edu.co:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=webgis_aedes:reportarsintoma_dolor_estomacal&maxFeatures=50&outputFormat=application%2Fjson'
        })
    })
);

sintoma.push(
    new ol.layer.Heatmap
    ({
        title: 'Fiebre',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://vigea.unillanos.edu.co:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=webgis_aedes:reportarsintoma_fiebre&maxFeatures=50&outputFormat=application%2Fjson'
        })
    })
);

sintoma.push(
    new ol.layer.Heatmap
    ({
        title: 'Tos',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://vigea.unillanos.edu.co:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=webgis_aedes:reportarsintoma_tos&maxFeatures=50&outputFormat=application%2Fjson'
        })
    })
);

sintoma.push(
    new ol.layer.Heatmap
    ({
        title: 'Sangrado',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://vigea.unillanos.edu.co:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=webgis_aedes:reportarsintoma_sangrado&maxFeatures=50&outputFormat=application%2Fjson'
        })
    })
);

sintoma.push(
    new ol.layer.Heatmap
    ({
        title: 'Todos los sintomas',
        visible: false,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://vigea.unillanos.edu.co:8080/geoserver/webgis_aedes/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=webgis_aedes:reportarsintoma_general&maxFeatures=50&outputFormat=application%2Fjson'
        })
    })
);

var groupSintoma = new ol.layer.Group({
    title: 'Reportes de sintomas',
    layers: sintoma
});
// TERMINA CAPA DE REPORTES SINTOMAS




var map = new ol.Map
({
    target: 'map',
    render: 'canvas',
    layers: [],
    view: new ol.View({
        center: ol.proj.fromLonLat([-73.63, 4.14]),
        zoom: 10
    })
});

map.addLayer(groupMapas);
map.addLayer(groupCriadero);
map.addLayer(groupEstadio);
map.addLayer(groupSintoma);

var layerSwitcher = new ol.control.LayerSwitcher({
    tipLabel: 'Leyenda'
});

map.addControl(layerSwitcher);
layerSwitcher.showPanel();
map.addControl(new ol.control.ZoomSlider());