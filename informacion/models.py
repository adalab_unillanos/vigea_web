# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.
class Noticia(models.Model):
    class Meta:
        verbose_name = 'noticia'
        verbose_name_plural = 'noticias'

    titulo = models.CharField(max_length=200)
    contenido = models.TextField()
    imagen = models.ImageField(blank=True, null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.titulo

class Prevencion(models.Model):
    class Meta:
        verbose_name = 'prevención'
        verbose_name_plural = 'prevenciones'
    titulo = models.CharField(max_length=200)
    contenido = models.TextField()
    imagen = models.ImageField(blank=True, null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.titulo

class Ayuda(models.Model):
    class Meta:
        verbose_name = 'ayuda'
        verbose_name_plural = 'ayudas'

    titulo = models.CharField(max_length=200)
    contenido = models.TextField()
    imagen = models.ImageField(blank=True, null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.titulo